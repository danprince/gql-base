// The knexfile tells knex how to connect to the database

module.exports = {
  // Tell knex to use the "pg" module to connect to the db
  client: "pg",

  // Tell knex where to find the db and use the default credentials
  connection: "postgres://postgres@localhost/postgres",
}
