// The "up" function moves your db schema from the current state into
// the desired state (e.g. creates new tables, or changes column types)
exports.up = function(knex) {
  return knex.schema.createTable("cities", table => {
    table.increments("id");
    table.integer("population");
    table.string("name");
  });
};

// The "down" function moves your db schema from the desired state back
// into the current state, so that you can move between the two if you
// need to make some more tweaks, or switch branch etc
exports.down = function(knex) {
  return knex.schema.dropTable("cities");
};
