exports.up = async function(knex) {
  await knex.schema.createTable("worlds", table => {
    table.increments("id");
    table.string("name");
  });

  await knex.schema.alterTable("cities", table => {
    table
      // an integer column called world_id
      .integer("world_id")

      // references the id column from the worlds table
      .references("worlds.id")

      // if the world it references changes id, update it here too
      .onUpdate("cascade")

      // if the world it references is deleted, then delete this too
      .onDelete("cascade");
  });
};

exports.down = async function(knex) {
  await knex.schema.table("cities", table => {
    table.dropColumn("world_id");
  });

  await knex.schema.dropTable("worlds");
};
