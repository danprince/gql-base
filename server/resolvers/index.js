const db = require("../db");

const resolvers = {
  Query: {
    async me(parent, args, context) {
      return context.user;
    },

    async cities(parent, args, context) {
      return db("cities").select("*");
    },

    async citiesByPopulation(parent, args, context) {
      return db("cities")
        .select("*")
        .whereBetween("population", [args.min, args.max]);
    },

    async world(parent, args, context) {
      return db("worlds").select("*").where("id", args.id).first();
    },

    async worlds(parent, args, context) {
      return db("worlds").select("*");
    }
  },

  Mutation: {
    async createCity(parent, args, context)  {
      let cities = await db("cities")
        .insert({
          name: args.name,
          population: args.population,
        })
        .returning("*");

      return cities[0];
    },

    async createWorld(parent, args, context) {
      let worlds = await db("worlds")
        .insert({ name: args.name })
        .returning("*");

      return worlds[0];
    },
  },

  City: {
    size(parent, args, context) {
      // parent is the "city" itself
      if (parent.population < 100) {
        return "Village";
      }

      if (parent.population < 1000) {
        return "Town";
      }

      if (parent.population < 100000) {
        return "City";
      }

      return "Capital";
    },
    async world(parent, args, context) {
      return db("worlds").select("*").where("id", parent.world_id).first();
    }
  },

  World: {
    cities(parent, args, context) {
      return db("cities").select("*").where("world_id", parent.id);
    }
  }
}

module.exports = resolvers;
