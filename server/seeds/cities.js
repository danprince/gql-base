exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex("cities").del();
  await knex("worlds").del();

  let [
    WESTEROS,
    MIDDLE_EARTH,
    REDANIA,
  ] = await knex("worlds").insert([
    { name: "Westeros" },
    { name: "Middle Earth" },
    { name: "Redania" },
  ]).returning("id");

  await knex("cities").insert([
    { name: "King's Landing", population: 100000, world_id: WESTEROS },
    { name: "Minas Tirith", population: 5000, world_id: MIDDLE_EARTH },
    { name: "Novigrad", population: 6050, world_id: REDANIA },
  ]);
};
