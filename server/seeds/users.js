exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex("users").del();

  return knex("users").insert([
    { name: "Ed" },
    { name: "Dan" },
  ]);
};
