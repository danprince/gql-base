const { ApolloServer, AuthenticationError, gql } = require("apollo-server");
const fs = require("fs");
const db = require("./db");
const resolvers = require("./resolvers");

// Read the schema from a graphql file
const typeDefs = gql(fs.readFileSync("./schema.graphql", "utf8"));

async function context(context) {
  let currentUserId = context.req.headers["authorization"];

  // Check that the user has actually passed an authorization header
  if (currentUserId == null) {
    throw new AuthenticationError("Missing authorization header");
  }

  // Get the user from the db based on the id authorization
  let user = await db("users")
      .select("*")
      .where("id", currentUserId)
      .first();

  // Send back an error if this id didn't refer to a user
  if (user == null) {
    throw new AuthenticationError("Invalid authentication");
  }

  // Return the user object so that resolvers can use it as
  // context.user
  return {
    user
  };
}

// Create a new GraphQL server and export it for other files to use
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context,
});

module.exports = server;
